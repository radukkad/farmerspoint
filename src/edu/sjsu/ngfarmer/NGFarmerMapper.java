package edu.sjsu.ngfarmer;
import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapred.OutputCollector;
import org.apache.hadoop.mapred.Reporter;

//public class NGFarmerMapper extends Mapper<LongWritable, Text, Text, DoubleWritable> {
public class NGFarmerMapper extends Mapper<LongWritable, Text, Text, Text> {
    public static int noRecords = 0;
	static Enum<?> NUM_RECORDS;
	DoubleWritable price = new DoubleWritable();
	Text strValue = new Text();
	Text strKey = new Text();

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapred.Mapper#map(java.lang.Object, java.lang.Object, org.apache.hadoop.mapred.OutputCollector, org.apache.hadoop.mapred.Reporter)
	 */
	protected void map(LongWritable _key, Text value,
			Context context)
			throws IOException, InterruptedException {

		String TempString = value.toString();
		String[] MarketData = TempString.split(",");
        ++noRecords;
        int index = 1;
    	try {
    		strKey.set(MarketData[index++]+","+MarketData[index++]+","+
    					MarketData[index++]+","+MarketData[index++]+
    									","+MarketData[index++]+",");
    		strValue.set(MarketData[0]+","+MarketData[index++]);

    		//price.set(Double.parseDouble(MarketData[index++]));
    		//context.write(str, price);
    		context.write(strKey, strValue);
    		//System.out.println("[map]**** Store record : "+strKey + " = "+strValue);
    	} catch (NumberFormatException e) {e.printStackTrace(); }
        
	}
}
