package edu.sjsu.ngfarmer;

// imported jars
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class NGFarmerDriver extends Configured implements Tool {
	
	@Override
	  public int run(String[] args) throws Exception {
		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args).getRemainingArgs();
		if (otherArgs.length != 2) {
		      System.err.println("Usage: NGFarmerDriver <input> <output>");
		      System.exit(2);
		}
	    Job job = new Job(conf, "ngfarmer" );
	    job.setJarByClass(NGFarmerDriver.class);
	    job.setJobName(getClass().getSimpleName());
	 
	    FileInputFormat.addInputPath(job, new Path(args[0]));
	    FileOutputFormat.setOutputPath(job, new Path(args[1]));
	 
	    job.setMapperClass(NGFarmerMapper.class);
	    job.setCombinerClass(NGFarmeReducer.class);
	    job.setReducerClass(NGFarmeReducer.class);
	 
	    job.setOutputKeyClass(Text.class);
	    job.setOutputValueClass(Text.class);
	    //job.setOutputValueClass(DoubleWritable.class);

	    return job.waitForCompletion(true) ? 0 : 1;
	  }
	 
	  public static void main(String[] args) throws Exception {
	    int rc = ToolRunner.run(new NGFarmerDriver(), args);
	    System.exit(rc);
	  }
}





