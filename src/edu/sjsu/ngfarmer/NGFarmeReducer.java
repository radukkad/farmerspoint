package edu.sjsu.ngfarmer;


import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.Text;
//import org.apache.hadoop.mapred.OutputCollector;
//import org.apache.hadoop.mapred.Reporter;
import org.apache.hadoop.mapreduce.Reducer;

public class NGFarmeReducer extends Reducer <Text, Text, Text, Text> {

	/* (non-Javadoc)
	 * @see org.apache.hadoop.mapred.Reducer#reduce(java.lang.Object, java.util.Iterator, 
	 * 						org.apache.hadoop.mapred.OutputCollector, org.apache.hadoop.mapred.Reporter)
	 */
	public void reduce(Text _key, Iterable<Text> values, Context context)
											throws IOException, InterruptedException {
		Text key = _key;
		double maxPrice=0.0;
		double frequencyPrice = 0.0;
		String cellNumber="";
		String farmerIdByBestPrice="";
		String price = "";
		//System.out.println("Reducer: key = "+_key);
		HashMap map = new HashMap();
		
		//Bengal Grams(Gram),Kayamkulam,Alappuzha,Kerala,01/18/2014,	40820359621,33.0
	    for (Text val : values) {
			//System.out.println("Reducer: Value = "+val);
	    	//maxPrice = Math.max(maxPrice, val.get());
			String value = val.toString();
			cellNumber = value.substring(0, value.indexOf(','));
			price = value.substring(value.indexOf(',')+1);
			frequencyPrice = Double.parseDouble(price);
			if (frequencyPrice > maxPrice) {
				maxPrice = frequencyPrice;
			    map.put(maxPrice, cellNumber);
			}
			//maxPrice = Math.max(maxPrice, frequencyPrice);
		}
	    
	    /*Retrieve the cellnumber associated to the max price*/
	    cellNumber = (String)map.get(maxPrice);
		Text txtValue = new Text(cellNumber+","+Double.toString(maxPrice));
		//context.write(key, new DoubleWritable(maxPrice));
		context.write(key, txtValue);
	}
}
